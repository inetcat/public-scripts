#!/usr/bin/python3

from netmiko import ConnectHandler
import getpass

# Define variables for use in fortigate Dictionary
ip = str(input('Enter device IP: '))
user = input('Enter your username: ')
password = getpass.getpass(prompt = 'RADIUS password: ')
hostname = input('Enter circuit reference (e.g. ACC123456): ')

# Dictionary that is referenced by ConnectHandler
fortigate = {
    'device_type': 'fortinet',
    'host':   ip,
    'username': user,
    'password': password,
    'port' : 22,
}
net_connect = ConnectHandler(**fortigate)

# Sequence of commands to display list of VDOMs on device
vdom_list_commands = [ 'config global',
                      'diagnose  sys vd list',
                      'end']

# Sequence of commands to show firewall policies of a VDOM. Second item is replaced under the While loop
vdom_policy_commands = [ 'config vdom',
                    'edit VDOM REFERENCE',
                    'show firewall policy' ]

# Define variables for use in the main if/else statement
vdcheck = net_connect.send_command('get system status')
vdom = ''
output = ''
arbitrary = 0

# Main script here
if "Virtual domain configuration: disable" in vdcheck:
    output = net_connect.send_command('show firewall policy')
else:
    print('This device is using VDOMs')
    while arbitrary == 0:
        vdom = input('Please enter a valid VDOM reference: ')
        vdlist = net_connect.send_config_set(vdom_list_commands)
        if vdom in vdlist:
            vdom_policy_commands[1] = 'edit ' + vdom
            output = net_connect.send_config_set(vdom_policy_commands)
            arbitrary += 1
        else:
            print('No matching VDOM reference on this firewall. ')
            arbitrary = 0

# Write output to new .cfg file
f = open(hostname + ".cfg", "x")
f.write(output)
f.close()

print(hostname + '.cfg created successfully')
